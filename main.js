
// DESCRIPTION
document.querySelector('.nameRestaurant').innerHTML = "<h1 class=\"title\">" + contenu.name + "</h1>";
document.querySelector('.descriptionRestaurant').innerHTML = "<p>" + contenu.description+ "</p>";


// CARTE DES PLATS
var menuCarte = contenu.carte;
var carte = document.querySelector('.carte');
console.log(menuCarte);
menuCarte.forEach(function(item){
  carte.innerHTML = carte.innerHTML + '<div id="plats" data-id=\"'+ item.id +'\">'
                                  + '<div class="platImg"><img src=\"'+ item.image +'\"alt=""></div>'
                                  + '<div class=\"platContenu\"><h3>' + item.name + '</h3>'
                                  + '<p>' + item.description + '</p>'
                                  + '<p> Prix : ' + item.price + '</p>'
                                  + '<div onclick="addItem(this)" class="addButton"><h3>+</h3></div>'
                                  + '</div>';
})


var addButton = document.querySelector('.addButton');

var dropDown = document.querySelector('.dropDown');
var quantity = new Array();


// CRÉATION DE BOITE POUR CHAQUE ELEMENT DE LA CARTE
for (var i = 0; i < contenu.carte.length; i++) {
  dropDown.innerHTML = '<div class=\"itemPanier\"><h3 class=\"itemPanierName\"></h3><h3 class=\"itemPanierPrice\"></h3><h3 class=\"itemPanierQuantity\"></h3><h3 class=\"itemTotal\"></h3></div>' + dropDown.innerHTML
}


var itemPanier = document.getElementsByClassName('itemPanier');
var itemPanierName = document.getElementsByClassName('itemPanierName');
var itemPanierPrice = document.getElementsByClassName('itemPanierPrice');
var itemPanierQuantity = document.getElementsByClassName('itemPanierQuantity');
var panierPriceTotal = document.getElementsByClassName('panierPriceTotal');
var priceTotalArray = new Array();
itemPanier[0].style.display = "none";
itemPanier[1].style.display = "none";
itemPanier[2].style.display = "none";



// RECHERCHE LES DATAS DANS CONTENU
function getDataById(a){
    return contenu.carte.filter(function(item){
      return item.id == a;
    })
}

// CREATION D'UN TABLEAU DE QUANTITÉ POUR CHAQUE ITEM
for (var i = 0; i < contenu.carte.length; i++) {
  quantity[i] = 0;
  priceTotalArray[i] = 0;
};


//FONCTION D'INTÉ D'ITEM
//--------------------
var platPanier = new Object();
// ajout de l'item a platPanier
function addItem(param){
  var idPlat = param.parentElement.parentElement.dataset.id;
  var plat = getDataById(idPlat);
  var itemPrice = parseInt(plat[0].price.split(' ')[0])
console.log('itemPrice :',itemPrice)
swal({   title: "Parfait! Votre commande a été prise!",   text: "Vous venez de commander: " + plat[0].name + "  - Veuillez verifier le panier s'il vous plait",   imageUrl: "resto.png" });
if (quantity[idPlat] == 0) {
  platPanier.name = plat[0].name;
  platPanier.price = plat[0].price;
  quantity[idPlat] = 1;
}else if (quantity[idPlat] > 0) {
  quantity[idPlat] += 1;
  platPanier.name = plat[0].name;
  platPanier.price = plat[0].price;

}


// CALCUL DU PRIX TOTAL D'UN ITEM
var itemPanierPriceTotal = new Array();
itemPanierPriceTotal[idPlat] = quantity[idPlat] * itemPrice;
console.log('platPanier: ',platPanier);

// INTEGRATION DES ITEM
itemPanierName[idPlat].innerHTML = platPanier.name;
itemPanierQuantity[idPlat].innerHTML = quantity[idPlat];
itemPanierPrice[idPlat].innerHTML = platPanier.price;
document.getElementsByClassName('itemTotal')[idPlat].innerHTML = itemPanierPriceTotal[idPlat] +' €';
console.log('quantity :',quantity)
if (quantity[0]===0 && quantity[1]===0 && quantity[2]===0) {
  itemPanier[0].style.display = "none";
  itemPanier[1].style.display = "none";
  itemPanier[2].style.display = "none";
}else if (quantity[0]>0 && quantity[1]===0 && quantity[2]===0) {
  itemPanier[0].style.display = "flex";
  itemPanier[1].style.display = "none";
  itemPanier[2].style.display = "none";
}else if (quantity[0]===0 && quantity[1]>0 && quantity[2]===0) {
  itemPanier[0].style.display = "none";
  itemPanier[1].style.display = "flex";
  itemPanier[2].style.display = "none";
}else if (quantity[0]===0 && quantity[1]===0 && quantity[2]>0) {
  itemPanier[0].style.display = "none";
  itemPanier[1].style.display = "none";
  itemPanier[2].style.display = "flex";
}else if (quantity[0]>0 && quantity[1]>0 && quantity[2]===0) {
  itemPanier[0].style.display = "flex";
  itemPanier[1].style.display = "flex";
  itemPanier[2].style.display = "none";
}else if (quantity[0]>0 && quantity[1]===0 && quantity[2]>0) {
  itemPanier[0].style.display = "flex";
  itemPanier[1].style.display = "none";
  itemPanier[2].style.display = "flex";
}else if (quantity[0]===0 && quantity[1]>0 && quantity[2]>0) {
  itemPanier[0].style.display = "none";
  itemPanier[1].style.display = "flex";
  itemPanier[2].style.display = "flex";
}else if (quantity[0]>0 && quantity[1]>0 && quantity[2]>0) {
  itemPanier[0].style.display = "flex";
  itemPanier[1].style.display = "flex";
  itemPanier[2].style.display = "flex";
}
priceTotalArray[idPlat] = itemPanierPriceTotal[idPlat]
console.log('priceTotalArray: ',priceTotalArray);
var priceTotal = 0;
for (var i = 0; i < priceTotalArray.length; i++) {
  priceTotal += priceTotalArray[i];
}
console.log('pricetotal: ',priceTotal)
panierPriceTotal[0].innerHTML = priceTotal + '€'
}
console.log(quantity[0])

// CLICK DU PANIER
var panier = document.getElementsByClassName('panier')[0];
var nclick = 0
panier.onclick = function(){
  console.log(panier);
  if (nclick == 0) {
    dropDown.style.opacity = 1;
    nclick = 1;
  }else if (nclick == 1) {
    dropDown.style.opacity = 0;
    nclick = 0;
  }
}


//ANIMATION SCROOL

window.sr = ScrollReveal();
sr.reveal('#plats');
